'''
RETO N° 3
C1-G18 FUNDAMENTOS DE PROGRAMAMCIÓN 
ESTUDIANTE: Juan Antonio Ceballos Usuga

La universidad Centro Americana desea realizar un programa que le permita llevar el registro de 
todos los estudiantes. La información que maneja por cada estudiante es:
1. Nombre, apellidos, dirección, teléfono, email y código
2. Asignaturas matriculadas en el semestre
3. Contactos: Nombre y teléfono
4. Notas obtenidas en el semestre actual.

El programa debe permitir:
1. Registrar la información del estudiante
2. Consultar estudiantes por código.
3. Consultar las notas de un determinado estudiante.
4. Buscar las notas de un estudiante.
5. Listar los estudiantes matriculados.

Nota: El programa debe ser resuelto usando Listas.

ESTRUCTURA DE DATOS:

all_students = [[personal_info, contact_info,subjects]]

personal_info = [codigo,nombre,apellido,direccion,telefono,correo]
contact_info = [nombre,telefono]
subjects = [[asignatue,[calificaciones]]]

Ejemplo:

[	
	[7840441,'Juan','Ceballos','CL 64 # 41 - 59','3146910642','jacu29@gmail.com'], 
    ['Liliana Usuga','3217674103'], 
    [['Matematicas',[5.0,4.0]],['Fisica',[4.0,3.0,5.0]]]
]


'''

import os

#Variables gobales
#Servirán como base de datos en la ejecución del script
all_students = []

personal_info = None
contact_info = None
subjects = None

#Función para borrar la pantalla
def delete_screen():
    '''
    Funcion para realizar una limpieza de pantalla.
    Realizar una verificación con os.name del sistema operativo.
    '''
    if os.name == "posix":
        os.system ("clear")
    elif os.name == "ce" or os.name == "nt" or os.name == "dos":
        os.system ("cls")

#Menú principal
def main_menu():
    """
    Esta función genera el menú principal para poder interactuar con
    las posibilidades del prgrama
    """
    delete_screen()
    value = int(input('''
    BIENVENIDO AL SISTEMA INTEGRADO DE REGITROS DE ESTUDIANTES - SIRE

    1. Registrar Estudiante
    2. Buscar Estudiante
    3. Buscar Notas Estudiante
    4. Listar estudiantes
    0. Salir

    '''))

    return value

def registro_estudiante():
    delete_screen()
    print('REGISTRO DE ESTUDIANTE')

    personal_info = infromacion_estudiate()
    print('')
    contact_info = informacion_contacto()
    print('')
    asignaturas = asignaturas_calificaciones()
    input()
    all_students.append([personal_info,contact_info,asignaturas])

def infromacion_estudiate():
    print('INFORMACIÓN PERSONAL\n')
    # 
    while True:
        try:
            codigo = int(input('Código: '))
            break
        except ValueError:
            print('Debes ingresar un numero.') 

    nombre = input('Nombre: ')
    apellido = input('Apellido: ')
    direccion = input('Dirección: ')
    telefono = input('Teléfono: ')
    correo = input('Correo Electrónico: ')
    return [codigo,nombre,apellido,direccion,telefono,correo]

def informacion_contacto():
    print('INFORMACIÓM DE CONTACTO\n')
    nombre = input('Nombre Contacto: ')
    telefono = input('Teléfono: ')
    return [nombre,telefono]

def asignaturas_calificaciones():
    print('REGISTRO DE ASIGNATURAS\n')
    asignaturas = []
    agregar_asignatura = True
    while agregar_asignatura:
        calificaciones = []
        asignatura = input('Asignatura: ')
        agregar_calificaciones = True
        while agregar_calificaciones:
            while True:
                try:
                    calificacion = float(input('Nueva Calificación: '))
                    break
                except ValueError:
                    print('Debes ingresar un numero.') 

            nueva_caificacion = input('Deseas agregar otra calificación [S/N]: ')
            calificaciones.append(calificacion)
            if nueva_caificacion == 'N' or nueva_caificacion == 'n':
                agregar_calificaciones = False
        asignaturas.append([asignatura,calificaciones])
        nueva_asignatura = input('Desea agregar otra asigntura [S/N]: ')
        if nueva_asignatura == 'N' or nueva_asignatura == 'n':
            agregar_asignatura = False

    return asignaturas

def buscar_estudiante(codigo):
    # print(codigo,'BUSQUEDA DE ESTUDIANTE')
    if len(all_students) > 0:
            for student in all_students:
                if student[0][0] == codigo:
                    print(f'Código Estudiantil: {student[0][0]}')
                    print('INFORMACIÓN PERSONAL\n')
                    print(f''' 
Nombre: {student[0][1]} {student[0][2]}
Dirección: {student[0][3]}
Teléfono: {student[0][4]}
Correo Electrónico: {student[0][5]}
                    ''')
                    print('INFORMACIÓN DE CONTACTO\n')
                    print(f'''
Nombre de contacto: {student[1][0]}
Télefono: {student[1][1]}
                    ''')
                    print('MATERIAS MATRICULADAS\n')
                    for subject in student[2]:
                        print(subject[0]+': ',subject[1])
                else:
                    print(f'No hay coincidencias para el código {codigo}')
    else:
        print('No hay estudiantes registrados.')
                    
    input()

def buscar_calificaciones(codigo):
    if len(all_students) > 0:
        for student in all_students:
            if student[0][0] == codigo:
                delete_screen()
                print(f'Código estudiantil: {codigo}')
                print(f'Estudiante: {student[0][1]} {student[0][2]}')
                for subject in student[2]:
                    print(subject[0]+': ',subject[1])
            else:
                print(f'No hay coincidencias para el código {codigo}')
    else:
        print('No hay estudiantes registrados.')
    input()


def listar_estudiantes():
    delete_screen()
    print('ESTUDIANTES\n')
    for student in all_students:
        print(f'Código: {student[0][0]}')
        print(f'Estudiante: {student[0][1]} {student[0][2]}')
        print('Asignaturas: ', end=' ')
        for subject in student[2]:
            print(subject[0], end=' ')
        print('\n')
    input()

if __name__ == '__main__':
    menu = True
    while menu:
        value = main_menu()

        if value == 1:
            registro_estudiante()

        elif value == 2:
            delete_screen()
            print('BUSCANDO ESTUDIANTE')
            while True:
                try:
                    codigo = int(input('Ingrese el código del estudiante: '))
                    break
                except ValueError:
                    print('Debes ingresar un numero.') 
            buscar_estudiante(codigo)

        elif value == 3:
            delete_screen()
            print('BUSCANDO NOTAS DEL ESTUDIANTE')
            while True:
                try:
                    codigo = int(input('Ingrese el código del estudiante: '))
                    break
                except ValueError:
                    print('Debes ingresar un numero.') 
            buscar_calificaciones(codigo)

        elif value == 4:
            listar_estudiantes()

        elif value == 0:
            menu = False
            input('Hasta Pronto')
        else:
            input('Opción no válida.')